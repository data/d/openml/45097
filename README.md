# OpenML dataset: MLL

https://www.openml.org/d/45097

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**MLL dataset**

**Authors**: S. Armstrong, J. Staunton, L. Silverman, R. Pieters, M. den Boer, M. Minden, S. Sallan, E. Lander, T. Golub, S. Korsmeyer, et al

**Please cite** ([URL](https://pubmed.ncbi.nlm.nih.gov/11731795/)): S. Armstrong, J. Staunton, L. Silverman, R. Pieters, M. den Boer, M. Minden, S. Sallan, E. Lander, T. Golub, S. Korsmeyer, et al, Mll translocations specify a distinct gene expression profile that distinguishes a unique leukemia, Nat. Genet. 30 (1) (2002) 41-47.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45097) of an [OpenML dataset](https://www.openml.org/d/45097). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45097/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45097/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45097/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

